# PyTCPScan
<img alt="AppVeyor" src="https://img.shields.io/appveyor/ci/werdelesmarcio/PyTCPScan">  [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=werdelesmarcio_PyTCPScan&metric=alert_status)](https://sonarcloud.io/dashboard?id=werdelesmarcio_PyTCPScan)  <img alt="GitHub code size in bytes" src="https://img.shields.io/github/languages/code-size/werdelesmarcio/PyTCPScan">  <img alt="GitHub" src="https://img.shields.io/github/license/werdelesmarcio/PyTCPScan">

Repository for the PyTCPScan application.

PyTCPScan consists of an application focused on GNU / Linux systems and its objective is to run a simple port scanner, where it verifies which ports are open by returning STATUS OPEN, for this it is necessary to pass the arguments that consist of the host address to be analyzed , followed by the range of doors between the start and end.

NOTE: If your goal is to check only one port, you must define the same value for the last two parameters.

This is version 1.1 and is still in development.


---

NOTE: To use as an executable, remember to give execute permission
**sudo chmod +x pytcpscan.py**

## Usage 
To run the application, the arguments must be passed with the target host, the starting port and the ending port. PyTCPScan will check which ports in the range have Open Status.

**./pytcpscan [target] [init_port] [final_port]**

```diff
             ""8"" 8""""8 8""""8 8""""8                   
  eeeee e    e 8   8    " 8    8 8      eeee eeeee eeeee 
  8   8 8    8 8e  8e     8eeee8 8eeeee 8  8 8   8 8   8 
  8eee8 8eeee8 88  88     88         88 8e   8eee8 8e  8 
  88      88   88  88   e 88     e   88 88   88  8 88  8 
  88      88   88  88eee8 88     8eee88 88e8 88  8 88  8 
----------------------------------------------------------
                                       PyTCPScan ver 1.1 
                                               By: GH05T 
                                    VAULT CYBER SECURITY 
----------------------------------------------------------
 [*]Connecting to Target: xxxxxxxxxxxx.com.br
 [*]Scanning ports between 21 and 110 ...
 
 [!]Please wait, scanning remote host XXX.XX.XXX.XXX
 [*]This may take a while, be patient.
 
 [+]POSITIVE TO Port 21:	Status: OPEN
 [+]POSITIVE TO Port 22:	Status: OPEN
 [+]POSITIVE TO Port 80:	Status: OPEN
--------------------------------------------------
 [!]Scanning Completed in:  0:00:44.584693
--------------------------------------------------

 ---Finished---

```

***The source code is under the GPL License.***


<img alt="GitHub last commit" src="https://img.shields.io/github/last-commit/werdelesmarcio/PyTCPScan?style=for-the-badge">   <img alt="GitHub contributors" src="https://img.shields.io/github/contributors/werdelesmarcio/PyTCPScan?style=for-the-badge">


<img src = "https://juststickers.in/wp-content/uploads/2017/08/powered-by-linux.png" width =150 align="middle">      <img src = "http://www.acornsoftware.net/images/OpenSource/opensourche.png" width=100 align="middle">
